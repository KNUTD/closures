let makeCounter = (start = 0, step = 1, direction = true) => {
    let currValue = start;
    return function() {
        let returnValue = currValue;
        (direction === true) ? currValue += step : currValue -= step;
        return returnValue;
    };
};
let outFirst = makeCounter(100, undefined, false);
let outSecond = makeCounter();
console.log(outFirst());
console.log(outFirst());
console.log(outSecond());
console.log(outSecond());

outFirst = null;
console.log(outFirst);
console.log(outSecond());